module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaFeatures: {
      legacyDecorators: true
    }
  },
  extends: ['@nuxtjs', 'plugin:nuxt/recommended'],
  plugins: [],
  // add your custom rules here
  ignorePatterns: ['static/'],
  rules: {
    'func-call-spacing': 'off',
    'no-tabs': 'off',
    'no-mixed-spaces-and-tabs': 'off'
  }
}
