export default {
  /*
	 ** Nuxt rendering mode
	 ** See https://nuxtjs.org/api/configuration-mode
	 */
  mode: 'universal',
  /*
	 ** Nuxt target
	 ** See https://nuxtjs.org/api/configuration-target
	 */
  target: 'static',
  /*
	 ** Headers of the page
	 ** See https://nuxtjs.org/api/configuration-head
	 */
  head: {
    title: process.env.npm_package_name || '',
    meta: [{
      charset: 'utf-8'
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    },
    {
      hid: 'description',
      name: 'description',
      content: process.env.npm_package_description || ''
    }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },
  /*
	 ** Server Middleware
	 */
  // serverMiddleware: [
  //   '~api/index'
  // ],
  serverMiddleware: {
    '/api': '~/api'
  },

  /*
	 ** Global CSS
	 */
  css: [{
    src: 'ant-design-vue/dist/antd.less',
    lang: 'less'
  },
  'quill/dist/quill.snow.css',
  'quill/dist/quill.bubble.css',
  'quill/dist/quill.core.css',
  '@assets/app.scss'
  ],
  /*
	 ** Plugins to load before mounting the App
	 ** https://nuxtjs.org/guide/plugins
	 */
  plugins: [
    '@/plugins/antd-ui',
    '@/plugins/vuex-orm-axios',
    {
      src: '~plugins/vue-quill-editor.js',
      ssr: false
    },
    '@/plugins/notification'
  ],
  /*
	 ** Auto import components
	 ** See https://nuxtjs.org/api/configuration-components
	 */
  components: true,
  /*
	 ** Nuxt.js dev-modules
	 */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment',
    ['@nuxtjs/moment', {
      defaultLocale: 'en'
    }]
  ],
  /*
	 ** Nuxt.js modules
	 */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxt/http'
  ],
  /*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
  axios: {
    headers: {
      common: {
        'Access-Control-Allow-Origin': '*'
      }
    }

  },
  /*
	 ** Build configuration
	 ** See https://nuxtjs.org/api/configuration-build/
	 */
  build: {
    /*
		 ** You can extend webpack config here
		 */
    // extend (config, ctx) {
    //   config.node = {
    //     fs: 'empty'
    //   }
    // },
    loaders: {
      less: {
        javascriptEnabled: true,
        modifyVars: {
          'primary-color': '#3cc5e0',
          'error-color': '#fb8701',
          'default-color': '#e4e4e4'
        }
      }
    }
  }
}
