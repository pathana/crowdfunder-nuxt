const mongoose = require('mongoose')
const validate = require('mongoose-validator')
const connection = require('../mongoose_connection')
const Schema = mongoose.Schema

const emailValidator = [
  validate({
    validator: 'isEmail'
  })
]

const memberSchema = new Schema({
  type: {
    type: String,
    enum: ['Leadership', 'Team Member', 'Advisor'],
    required: true
  },
  fullname: {
    type: String
  },
  role: String,
  email: {
    type: String,
    required: true,
    validate: emailValidator
  },
  bio: String,
  is_admin: Boolean,
  is_visible: Boolean,
  avatar: String,
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company'
  }
})

const MemberModel = connection.model('Member', memberSchema)

module.exports = MemberModel
