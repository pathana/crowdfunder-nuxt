const mongoose = require('mongoose')
const validate = require('mongoose-validator')
const connection = require('../mongoose_connection')
const Schema = mongoose.Schema

const emailValidator = [
  validate({
    validator: 'isEmail'
  })
]

const urlValidator = [
  validate({
    validator: 'isURL'
  })
]

const documentSchema = new Schema({
  type: String,
  file: String,
  name: String,
  access: Boolean
})

const investorSchema = new Schema({
  type: String,
  name: String
})

const fundingSchema = new Schema({
  source: String,
  type: String,
  amount: Number,
  date: Date
})

const dealSchema = new Schema({
  type: String,
  exemption: String,
  invest_button: String,
  valuation: Number,
  warrant_coverage: Number,
  return: String,
  return_percentage: Number,
  payment_frequency: String,
  payback_start_date: String,
  exact_date: Date,
  number_of_month: Number
})

const detailSchema = new Schema({
  campaign: String,
  goal: Number,
  maximum: Number,
  opened: Date,
  warrant_coverage: Number,
  stage: String,
  minimum: Number,
  dtime: Date,
  is_fundraise: Boolean,
  amount: Number,
  conversion_descount: Number
})

const tractionSchema = new Schema({
  dtime: {
    type: Date,
    required: true
  },
  data: String
})

const pressSchema = new Schema({
  title: String,
  url: {
    type: String,
    required: true,
    validate: urlValidator
  }
})

const partnerSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  avatar: String
})

const companySchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  url: {
    type: String,
    required: true,
    unique: true
  },
  country: String,
  city: String,
  tagline: String,
  industry: Array,
  keywords: Array,
  email: {
    type: String,
    validate: emailValidator
  },
  website: {
    type: String,
    validate: urlValidator
  },
  mobile: String,
  linked_in: String,
  facebook: String,
  twitter: String,
  logo: String,
  featured_img: String,
  video_url: String,
  slide_share: String,
  overview: String,
  content_title: String,
  content_data: String,
  members: [{
    type: Schema.Types.ObjectId,
    ref: 'Member'
  }],
  tractions: [{
    type: tractionSchema
  }],
  press: [{
    type: pressSchema
  }],
  partners: [{
    type: partnerSchema
  }],
  deal: {
    type: dealSchema,
    default: {}
  },
  detail: {
    type: detailSchema,
    default: {}
  },
  funding: [{
    type: fundingSchema
  }],
  highlights: [String],
  elevator: String,
  investors: [{
    type: investorSchema
  }],
  documents: [{
    type: documentSchema
  }]
})

const CompanyModel = connection.model('Company', companySchema)

module.exports = CompanyModel
