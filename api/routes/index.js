const companyRouter = require('./company.route')
const uploadRouter = require('./upload.route')
const memberRouter = require('./member.route')

module.exports = [
  companyRouter,
  uploadRouter,
  memberRouter
]
