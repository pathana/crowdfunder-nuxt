const {
  Router
} = require('express')
const controller = require('../controllers/company.controller')
const companyRouter = Router()

companyRouter.get('/companies', controller.companies)
companyRouter.get('/company', controller.company)
companyRouter.get('/company/:id', controller.company)
companyRouter.put('/company/:id', controller.update)
companyRouter.post('/company', controller.create)

module.exports = companyRouter
