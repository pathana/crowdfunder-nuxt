const {
  Router
} = require('express')
const controller = require('../controllers/company.controller')
const uploadRouter = Router()

uploadRouter.post('/upload/:id', controller.upload)
uploadRouter.post('/upload', (req, res) => {
  if (!req.files) {
    res.json({
      status: false,
      message: 'No File uploaded'
    })
  }

  const file = req.files.file
  const fileName = Date.now() + file.name
  file.mv('./uploads/' + fileName)

  res.json({
    filename: fileName
  })
})

module.exports = uploadRouter
