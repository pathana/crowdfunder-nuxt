const {
  Router
} = require('express')
const controller = require('../controllers/member.controller')
const memberRouter = Router()

memberRouter.post('/member', controller.create)

module.exports = memberRouter
