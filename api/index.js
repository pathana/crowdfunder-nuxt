// const fs = require('fs')
const express = require('express')
const fileUpload = require('express-fileupload')
const bodyParser = require('body-parser')
const app = express()
// const mongoose = require('mongoose')
// const multer = require('multer')
const routers = require('./routes')
// const connectionString = 'mongodb+srv://X2TRC63L5rl7py3a:X2TRC63L5rl7py3a@crowdfunder.it5gm.mongodb.net/development?retryWrites=true&w=majority'
// mongoose.connect(connectionString)

app.use(express.static('uploads'))
app.use(fileUpload({
  createParentPath: true
}))

// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(routers)

module.exports = app
