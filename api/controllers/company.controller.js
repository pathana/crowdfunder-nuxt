const to = require('await-to-js').default
const Company = require('../models/company.model')

exports.companies = async (req, res) => {
  const companies = await Company.find({})
  return res.json(companies)
}

exports.company = async (req, res) => {
  const {
    id
  } = req.params
  const [error, company] = await to(Company.findById(id).populate('members'))
  if (!error) {
    res.status(200).json(company)
  }
  res.status(500).json(error)
}

exports.create = async (req, res) => {
  const payload = req.body
  const company = new Company(payload)
  const [error, response] = await to(company.save())
  if (!error) {
    res.status(201).json(response)
  }
  res.status(401).json(error)
}

exports.update = async (req, res) => {
  const payload = req.body
  const {
    id
  } = req.params

  const company = await Company.findOneAndUpdate({
    _id: id
  }, {
    $set: payload
  })

  return res.json(company)
}

exports.upload = async (req, res) => {
  if (!req.files) {
    res.json({
      status: false,
      message: 'No File uploded'
    })
  }

  const {
    type
  } = req.body

  const file = req.files[type]
  const fileName = Date.now() + file.name
  file.mv('./uploads/' + fileName)

  const logo = {
    $set: {
      logo: fileName
    }
  }

  const featured = {
    $set: {
      featured_img: fileName
    }
  }

  const data = type === 'logo' ? logo : featured

  const {
    id
  } = req.params

  await Company.findByIdAndUpdate(id, data)
  const company = await Company.findById(id)
  return res.json(company)
}
