const to = require('await-to-js').default
const Member = require('../models/member.model')

exports.create = async (req, res) => {
  const payload = req.body
  if (req.files) {
    const file = req.files.avatar
    const fileName = Date.now() + file.name
    file.mv('./uploads/' + fileName)
    payload.avatar = fileName
  }
  const member = new Member(payload)
  const [error, response] = await to(member.save())
  if (!error) {
    res.status(201).json(response)
  }
  res.status(400).json(error)
}
