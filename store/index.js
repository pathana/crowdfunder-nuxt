// import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import VuexORMAxios from '@vuex-orm/plugin-axios'
import Company from '@/models/company.model'
import Country from '@/models/country.model'
import Member from '@/models/member.model'
import Industry from '@/models/industry.model'

VuexORM.use(VuexORMAxios)

const database = new VuexORM.Database()

database.register(Company)
database.register(Member)
database.register(Country)
database.register(Industry)

export const plugins = [VuexORM.install(database)]
