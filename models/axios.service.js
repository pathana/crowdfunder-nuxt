import to from 'await-to-js'

const axiosPost = async (url, payload) => {
  const [error, result] = await to(window.$nuxt.$axios.post(url, payload))
  if (error) {
    window.$nuxt.$errorNotification(error.response)
    return (error.response)
  } else {
    return result.data
  }
}

const axiosPut = async (url, payload) => {
  const [error, result] = await to(window.$nuxt.$axios.put(url, payload))
  if (error) {
    window.$nuxt.$errorNotification(error.response)
    return (error.response)
  } else {
    return result.response
  }
}

export default {
  axiosPost,
  axiosPut
}
