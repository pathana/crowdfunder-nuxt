import {
  Model
} from '@vuex-orm/core'
import Axios from '@/models/axios.service'

export default class Company extends Model {
	static entity = 'companies'

	static fields () {
	  return {
	    _id: this.string(''),
	    name: this.string(''),
	    url: this.string(''),
	    country: this.string(''),
	    city: this.string(''),
	    tagline: this.string(''),
	    industry: this.attr([]),
	    keywords: this.attr([]),
	    email: this.string(''),
	    website: this.string(''),
	    mobile: this.string(''),
	    linked_in: this.string(''),
	    facebook: this.string(''),
	    twitter: this.string(''),
	    logo: this.string(''),
	    featured_img: this.string(''),
	    video_url: this.string(''),
	    slide_share: this.string(''),
	    overview: this.string(''),
	    members: this.attr([]),
	    tractions: this.attr([]),
	    press: this.attr([]),
	    partners: this.attr([]),
	    content_data: this.string(''),
	    content_title: this.string(''),
	    detail: this.attr({}),
	    deal: this.attr({}),
	    funding: this.attr([]),
	    highlights: this.attr([]),
	    elevator: this.string(''),
	    investors: this.attr([]),
	    documents: this.attr([])
	  }
	}

	static labels () {
	  return {
	    name: 'Company Name',
	    url: 'Crowdfunder URL',
	    location: 'Company Location',
	    tagline: 'Company Tagline',
	    industry: 'Company Industry',
	    keywords: 'Enter Keywords',
	    email: 'E-mail Contact',
	    website: 'Website URL',
	    mobile: 'Mobile Number',
	    linked_in: 'Linked In',
	    facebook: 'Facebook',
	    twitter: 'Twitter',
	    logo: 'LOGO',
	    featured_img: 'FEATURED IMAGE',
	    video_url: 'Company Video',
	    slide_share: 'Company SlideShare',
	    overview: 'Company Overview',
	    custom_content: 'Custom Content'
	  }
	}

	static getAll () {
	  this.deleteAll()
	  return this.api().get('/api/companies')
	}

	static findById (id) {
	  this.deleteAll()
	  return this.api().get('/api/company/' + id)
	}

	static async createToDb (payload) {
	  this.deleteAll()
	  const result = await Axios.axiosPost('/api/company', payload)
	  return result
	}

	static updateToDb (payload, id) {
	  this.deleteAll()
	  return Axios.axiosPut('/api/company/' + id, payload)
	  // return this.api().put('/api/company/' + id, payload)
	}

	static async uploadFile (payload, id) {
	  return await Axios.axiosPost('/api/upload/' + id, payload)
	  // return this.api().post('/api/upload/' + id, payload)
	}
}
