import {
  Model
} from '@vuex-orm/core'
import {
  uniqBy
} from 'lodash'
// import * as world from '@/assets/world.json'
import * as worldCities from '@/assets/world-cities.json'

export default class Country extends Model {
	static entity = 'countries'

	static fields () {
	  return {
	    name: this.string('')
	  }
	}

	static async getCountries (search) {
	  const worlds = await uniqBy(worldCities.default, 'country')
	  const countries = worlds
	    .filter(w => w.country.toLowerCase().includes(search))
	    .map(c => c.country)
	  return countries
	}

	static async getCities (search, country) {
	  const countries = await uniqBy(worldCities.default, 'subcountry')
	  const cities = countries
	    .filter(w => (
	      w.country === country &&
				w.subcountry.toLowerCase().includes(search)
	    ))
	    .map(city => city.subcountry)
	  return cities
	}
}
