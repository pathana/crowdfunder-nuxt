import {
  Model
} from '@vuex-orm/core'
import Axios from '@/models/axios.service'

export default class Member extends Model {
	static entity = 'members'

	static fields () {
	  return {
	    _id: this.string(''),
	    fullname: this.string(''),
	    email: this.string(''),
	    type: this.string(''),
	    role: this.string(''),
	    bio: this.string(''),
	    is_admin: this.boolean(false),
	    is_visible: this.boolean(false),
	    avatar: this.string('')
	  }
	}

	static createToDb (payload) {
	  this.deleteAll()
	  return Axios.axiosPost('/api/member', payload)
	}
}
