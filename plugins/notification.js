import {
  notification
} from 'ant-design-vue'

export default (context, inject) => {
  inject('errorNotification', (error) => {
    const message = error.data.message
    notification.error({
      message: 'error',
      description: message
    })
  })
}
