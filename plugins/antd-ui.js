import Vue from 'vue'
import Antd, {
  FormModel
} from 'ant-design-vue'

Vue.use(Antd)
Vue.use(FormModel)
